# Exo

Ce formulaire est réalisé en Python à l'aide du Framework Flask.
Les informations sont stocké dans une base de données sqlite avec des queries pré-préparé, les mots de passe sont hashé.
Les condition d'acceptation du mot de passe sont d'utilisé des majuscules, minuscules, des chiffres et de ne pas contenir de caractère bannie.
Le bouton connect fait la connection a un utilisateur et validate une vérification de mot de passe.

## Utilisation

Installer Flask "pip3 install Flask", tout les autres librairies sont par défaut installé.
pour lancé le programme:
```
flask run
```
La page sera lancé sur le port 5000 de localhost.
http://127.0.0.1:5000

