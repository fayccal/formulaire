import sqlite3
import hashlib
from flask import Flask
from flask import render_template
from flask import request
import re


banned = ["\'", "-", "\"" "\\"]
getuserquery = "SELECT identifiant FROM userdata"
getpassquery = "SELECT password FROM userdata"
query1 = "CREATE TABLE IF NOT EXISTS userdata (identifiant TINYTEXT NOT NULL, password TINYTEXT NOT NULL);"
addquery = 'INSERT INTO userdata (identifiant, password) VALUES (?, ?);'
DATABASE = "user.db"
app = Flask("myapp")

@app.route("/")
def hello_index():
    return render_template('index.html', salutmsg="salut")

@app.route('/', methods = ['GET', 'POST'])
def validate():
    if request.method == 'POST':
        db = sqlite3.connect("user.db")
        cur = db.cursor()
        cur.execute(query1)
        db.commit()
        username = request.form["username"]
        password = request.form["password"]

        fetched = [id[0] for id in cur.execute(getuserquery)]
        print(fetched)
        fetchedpass = [passw[0] for passw in cur.execute(getpassquery)]

        cur.close()
        db.close()
        if request.form.get("resetb") != None:
            nothing = None
        elif request.form.get("valideb") != None:
            #if statements for validation button
            if username in fetched or username == "":
                return render_template('index.html', errormsg="Identifiant déja existant ou non autorisé")
            for x in banned:
                if x in username or x in password:
                    return render_template('index.html', errormsg="Caractère non autorisé présent")
            if bool(re.search(r'\d', password)) == False:
                return render_template('index.html', errormsg="Aucune chiffre n'est présent dans le mot de passe")
            if bool(re.match(r'\w*[A-Z]\w*', password)) == False:
                return render_template('index.html', errormsg="Aucune Majuscule n'est présent dans le mot de passe")
            if len(password) > 20 or len(password) < 8:
                return render_template('index.html', errormsg="Taille du mot de passe non respécté")

            return render_template('index.html', uservalue=username, passvalue=password, errormsg="C'est identifiant et mot de passe sont acceptable")
        elif request.form.get("addb") != None:
            #validé d'abord
            if valid_first(fetched, username, password) == False:
                return render_template('index.html',  errormsg="Les données fournites ne sont pas acceptable, veuillez faire validé vos informations")

            password = hashlib.sha512(password.encode()).hexdigest()
            db = sqlite3.connect(DATABASE)
            cur = db.cursor()
            cur.execute(addquery, (username, password))
            db.commit()
            cur.close()
            db.close()
            return render_template('index.html', errormsg="L\'utilisateur à été ajouté")
        elif request.form.get("connb") != None:
            if username in fetched:
                pos = fetched.index(username)
                #password = hashlib.sha512(password.encode()).hexdigest()
                if fetchedpass[pos] == hashlib.sha512(password.encode()).hexdigest():
                    return render_template('index.html', errormsg="Vous etes connecté")
                else:
                     return render_template('index.html', errormsg="Problème recommencé")
            else:
                    return render_template('index.html', errormsg="Problème recommencé")






def valid_first(fetched, username, password):
    if username in fetched or username == "":
        return False
    for x in banned:
        if x in username or x in password:
            return False
    if bool(re.search(r'\d', password)) == False:
        return False
    if bool(re.match(r'\w*[A-Z]\w*', password)) == False:
        return False
    if len(password) > 20 or len(password) < 8:
        return False
    return True
